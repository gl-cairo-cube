/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    cairo-rendering.c
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#include "geometry.h"
#include "cairo-rendering.h"
#include "globals.h"

#include <math.h>

void
draw_point (cairo_t* pCairoContext,
	    gdouble  fX,
	    gdouble  fY)
{
	cairo_set_line_width (pCairoContext, 0.015f);
	cairo_arc (pCairoContext, fX, fY, 0.025f, 0.0f, G_PI / 180.0f * 360.0f);
	cairo_set_source_rgba (pCairoContext, 0.5f, 1.0f, 0.5f, 0.5f);
	cairo_fill_preserve (pCairoContext);
	cairo_set_source_rgba (pCairoContext, 1.0f, 0.0f, 0.0f, 1.0f);
	cairo_stroke (pCairoContext);
}

void
render_curve (cairo_t* pCairoContext,
	      gint     iWidth,
	      gint     iHeight,
	      Line*    pLineOne,
	      Line*    pLineTwo)
{
	if (pLineOne == NULL || pLineTwo == NULL)
		return;

	cairo_save (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_SOURCE);
	cairo_scale (pCairoContext,
		     (gdouble) iWidth / 1.0f,
		     (gdouble) iHeight / 1.0f);
	cairo_set_source_rgba (pCairoContext, 0.0f, 0.0f, 0.0f, 0.0f);
	cairo_paint (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_OVER);

	cairo_set_source_rgba (pCairoContext, 0.0f, 0.0f, 0.0f, 1.0f);
	cairo_set_line_cap (pCairoContext, CAIRO_LINE_CAP_ROUND);
	cairo_set_line_width (pCairoContext, 0.05f);
	cairo_move_to (pCairoContext, pLineOne->start.fX, pLineOne->start.fY);
	cairo_curve_to (pCairoContext,
			pLineOne->end.fX,
			pLineOne->end.fY,
			pLineTwo->start.fX,
			pLineTwo->start.fY,
			pLineTwo->end.fX,
			pLineTwo->end.fY);
	cairo_stroke (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_OVER);
	cairo_set_line_width (pCairoContext, 0.01f);
	cairo_set_source_rgba (pCairoContext, 0.25f, 0.5f, 1.0f, 1.0f);
	cairo_move_to (pCairoContext, pLineOne->start.fX, pLineOne->start.fY);
	cairo_line_to (pCairoContext, pLineOne->end.fX, pLineOne->end.fY);
	cairo_stroke (pCairoContext);
	cairo_move_to (pCairoContext, pLineTwo->start.fX, pLineTwo->start.fY);
	cairo_line_to (pCairoContext, pLineTwo->end.fX, pLineTwo->end.fY);
	cairo_stroke (pCairoContext);
	draw_point (pCairoContext, pLineOne->start.fX, pLineOne->start.fY);
	draw_point (pCairoContext, pLineOne->end.fX, pLineOne->end.fY);
	draw_point (pCairoContext, pLineTwo->start.fX, pLineTwo->start.fY);
	draw_point (pCairoContext, pLineTwo->end.fX, pLineTwo->end.fY);
	cairo_restore (pCairoContext);
}

void
render_zini (cairo_t* pCairoContext,
	     gint     iWidth,
	     gint     iHeight)
{
	gdouble fLength = 1.0f / 25.0f;
	gdouble fY;
	gint i;

	cairo_save (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_SOURCE);
	cairo_set_line_cap (pCairoContext, CAIRO_LINE_CAP_ROUND);
	cairo_set_source_rgba (pCairoContext, 0.0f, 0.0f, 0.0f, 0.0f);
	cairo_scale (pCairoContext,
		     (gdouble) iWidth / 1.0f,
		     (gdouble) iHeight / 1.0f);
	cairo_paint (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_OVER);
	cairo_set_line_width (pCairoContext, fLength);
	for (i = 0; i < 60; i++)
	{
		cairo_save (pCairoContext);
		cairo_translate (pCairoContext, 0.5f, 0.5f);
		cairo_rotate (pCairoContext,
			      G_PI / 180.0f *
			      (g_ulMilliSeconds + 10.0f * i) *
			      0.36f);
		fY = 0.33f +
		     0.0825f *
		     sinf ((g_ulMilliSeconds + 10.0f * i) / 1000 * 10 * G_PI);
		cairo_translate (pCairoContext, 0.0f, fY);
		cairo_rotate (pCairoContext, G_PI / 180.0f * 6.0f * i);
		cairo_set_source_rgba (pCairoContext,
				       1.0f,
				       0.5f,
				       0.0f,
				       i * 0.01f);
		cairo_move_to (pCairoContext, -fLength, 0.0f);
		cairo_line_to (pCairoContext, fLength, 0.0f);
		cairo_stroke (pCairoContext);
		cairo_restore (pCairoContext);
	}
	cairo_restore (pCairoContext);
}

void
render_flower (cairo_t*	pCairoContext,
	       gint	iWidth,
	       gint	iHeight)
{
	cairo_save (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_SOURCE);
	cairo_set_source_rgba (pCairoContext, 0.0f, 0.0f, 0.0f, 0.0f);
	cairo_scale (pCairoContext,
		     (gdouble) iWidth / 1.0f,
		     (gdouble) iHeight / 1.0f);
	cairo_paint (pCairoContext);
	cairo_set_operator (pCairoContext, CAIRO_OPERATOR_OVER);

	cairo_translate (pCairoContext, 0.5f, 0.5f);
	cairo_rotate (pCairoContext, -G_PI / 180.0f * g_ulMilliSeconds * 0.36f);

	cairo_save (pCairoContext);

	cairo_translate (pCairoContext, -0.5f, -0.5f);
	cairo_move_to (pCairoContext, 0.5f, 0.4f);
	cairo_curve_to (pCairoContext, 0.5f, 0.2f, 0.6f, 0.1f, 0.7f, 0.1f);
	cairo_curve_to (pCairoContext, 0.8f, 0.1f, 0.9f, 0.2f, 0.9f, 0.3f);
	cairo_curve_to (pCairoContext, 0.9f, 0.4f, 0.8f, 0.5f, 0.6f, 0.5f);
	cairo_curve_to (pCairoContext, 0.8f, 0.5f, 0.9f, 0.6f, 0.9f, 0.7f);
	cairo_curve_to (pCairoContext, 0.9f, 0.8f, 0.8f, 0.9f, 0.7f, 0.9f);
	cairo_curve_to (pCairoContext, 0.6f, 0.9f, 0.5f, 0.8f, 0.5f, 0.6f);
	cairo_curve_to (pCairoContext, 0.5f, 0.8f, 0.4f, 0.9f, 0.3f, 0.9f);
	cairo_curve_to (pCairoContext, 0.2f, 0.9f, 0.1f, 0.8f, 0.1f, 0.7f);
	cairo_curve_to (pCairoContext, 0.1f, 0.6f, 0.2f, 0.5f, 0.3f, 0.5f);
	cairo_curve_to (pCairoContext, 0.2f, 0.5f, 0.1f, 0.4f, 0.1f, 0.3f);
	cairo_curve_to (pCairoContext, 0.1f, 0.2f, 0.2f, 0.1f, 0.3f, 0.1f);
	cairo_curve_to (pCairoContext, 0.4f, 0.1f, 0.5f, 0.2f, 0.5f, 0.3f);

	cairo_restore (pCairoContext);

	cairo_set_source_rgba (pCairoContext, 1.0f, 0.0f, 0.0f, 0.75f);
	cairo_fill (pCairoContext);
	cairo_restore (pCairoContext);
}

