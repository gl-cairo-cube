APP		=	gl-cairo-cube

CC		=	gcc

CFLAGS	=	-Wall -c -O3 -std=c99 \
			`pkg-config --cflags gtkglext-1.0`

LDFLAGS	=	`pkg-config --libs gtkglext-1.0` \
			-s -lGL -lGLU -lglut -lm

SRCS	=	geometry.c \
			opengl-rendering.c \
			cairo-rendering.c \
			events.c \
			main.c

OBJS	=	$(SRCS:.c=.o)

all: $(APP)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

# it is important that $(OBJS) stands _before_ $(LDFLAGS)
$(APP):	$(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o$(APP)

clean:
	rm -f *.o  *~ $(APP)

