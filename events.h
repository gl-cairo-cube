/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    events.h
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#ifndef _EVENTS_H
#define _EVENTS_H

#include <gtk/gtk.h>

gboolean
delete_handler (GtkWidget* pWidget,
		GdkEvent*  pEvent,
		gpointer   data);

gboolean
button_handler (GtkWidget*	pWidget,
		GdkEventButton*	pEvent,
		gpointer	data);

gboolean
scroll_handler (GtkWidget*	pWidget,
		GdkEventScroll*	pEvent,
		gpointer	data);

gboolean
motion_notify_handler (GtkWidget*      pWidget,
		       GdkEventMotion* pEvent,
		       gpointer	       data);

void
screen_changed_handler (GtkWidget* pWidget,
			GdkScreen* pOldScreen,
			gpointer   data);

void
realize_handler (GtkWidget* pWidget,
		 gpointer   data);

gboolean
configure_handler (GtkWidget*	      pWidget,
		   GdkEventConfigure* pEvent,
		   gpointer	      data);

gboolean
expose_handler (GtkWidget*	pWidget,
		GdkEventExpose*	pEvent,
		gpointer	data);

gboolean
key_press_handler (GtkWidget*	pWidget,
		   GdkEventKey*	pEvent,
		   GMainLoop*	pLoop);

gboolean
draw_handler (GtkWidget* pWidget);

#endif /* _EVENTS_H */

