/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    geometry.c
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#include "geometry.h"

void
advance (Point* pPoint)
{
	if (pPoint == NULL)
		return;

	if (pPoint->bGrowX)
	{
		if (pPoint->fX + pPoint->fStepX < pPoint->fUpperLimitX)
			pPoint->fX += pPoint->fStepX;
		else
		{
			pPoint->bGrowX = FALSE;
			pPoint->fX -= pPoint->fStepX;
		}
	}
	else
	{
		if (pPoint->fX - pPoint->fStepX > pPoint->fLowerLimitX)
			pPoint->fX -= pPoint->fStepX;
		else
		{
			pPoint->bGrowX = TRUE;
			pPoint->fX += pPoint->fStepX;
		}
	}

	if (pPoint->bGrowY)
	{
		if (pPoint->fY + pPoint->fStepY < pPoint->fUpperLimitY)
			pPoint->fY += pPoint->fStepY;
		else
		{
			pPoint->bGrowY = FALSE;
			pPoint->fY -= pPoint->fStepY;
		}
	}
	else
	{
		if (pPoint->fY - pPoint->fStepY > pPoint->fLowerLimitY)
			pPoint->fY -= pPoint->fStepY;
		else
		{
			pPoint->bGrowY = TRUE;
			pPoint->fY += pPoint->fStepY;
		}
	}
}

