/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    cairo-rendering.h
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#ifndef _CAIRO_RENDERING_H
#define _CAIRO_RENDERING_H

#include "geometry.h"

#include <gtk/gtk.h>

void
render_zini (cairo_t* pCairoContext,
	     gint     iWidth,
	     gint     iHeight);

void
render_curve (cairo_t* pCairoContext,
	      gint     iWidth,
	      gint     iHeight,
	      Line*    pLineOne,
	      Line*    pLineTwo);

void
render_flower (cairo_t*	pCairoContext,
	       gint	iWidth,
	       gint	iHeight);

#endif /* _CAIRO_RENDERING_H */

