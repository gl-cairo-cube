/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    globals.h
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <GL/gl.h>

#include "geometry.h"
#include "cairo-rendering.h"

#define FOVY_2 20.0
#define Z_NEAR 3.0

extern GTimer*	g_pTimerId;
extern gint	g_iWidth;
extern gint	g_iHeight;
extern cairo_t*	g_pCairoContext[];
extern guchar*	g_pucSurfaceData[];
extern gulong	g_ulMilliSeconds;
extern Line	g_lineOne;
extern Line	g_lineTwo;

