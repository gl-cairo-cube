/*******************************************************************************
**3456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
**      10        20        30        40        50        60        70        80
**
** file:
**    geometry.h
**
** author:
**    Mirco "MacSlow" Mueller <macslow@bangang.de>
**
** copyright (C) Mirco Mueller, 2006/2007, placed under the terms of the GPL
**
*******************************************************************************/

#ifndef _GEOMETRY_H
#define _GEOMETRY_H

#include <gtk/gtk.h>

typedef struct _Point
{
	gdouble	 fX;
	gdouble  fY;
	gboolean bGrowX;
	gboolean bGrowY;
	gdouble  fStepX;
	gdouble  fStepY;
	gdouble  fLowerLimitX;
	gdouble  fUpperLimitX;
	gdouble  fLowerLimitY;
	gdouble  fUpperLimitY;
} Point;

typedef struct _Line
{
	Point start;
	Point end;
} Line;

void
advance (Point* pPoint);

#endif /* _GEOMETRY_H */

